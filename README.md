# Onboarding Payment Core #

Create Social Media with MVVM-Coordinator design pattern and Functional Reactive Programming with RxSwift.

### Feature ###

* Show header on Home
* Show post on Home
* Show comment on Home
* Create post

### Tools ###

* Xcode 12.4
* Swift 5
* iOS 10.0+

### Dependencies ###

* RxSwift
* RxCocoa
* Texture

### Daily Progress ###

31 Agustus 2021

* Research Texture

1 September 2021

* Header Home

![](images/feature_header_home.png)

2 September 2021

* Post Home

![](images/feature_post_home.png)

3 September 2021

* Create Post
* Research RxSwift
* Refacator to RxSwift