//
//  TabBarCoordinator.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 02/09/21.
//

import UIKit

final class TabBarCoordinator: NSObject, Coordinator {
    var childCoordinators: [Coordinator]
    
    var navigationController: UINavigationController
    
    var finishDelegate: CoordinatorFinishDelegate?
    
    var tabBarController: UITabBarController
    
    var homeViewController: HomeViewController
    var createPostViewController: CreatePostViewController
    
    required init(_ navigationController: UINavigationController) {
        self.childCoordinators = []
        self.navigationController = navigationController
        self.tabBarController = .init()
        self.homeViewController = HomeViewController()
        self.createPostViewController = CreatePostViewController()
    }
    
    func start() {
        let homeVC = createTabController(self.homeViewController, title: "Home", image: .home, index: 0)
            homeVC.setNavigationBarHidden(true, animated: true)
        let dummyVC1 = createTabController(title: "My Network", image: .people, index: 1)
        let createPostVC = createTabController(self.createPostViewController, title: "Post", image: .addPost, index: 2)
        let dummyVC2 = createTabController(title: "Notifications", image: .notification, index: 3)
        let dummyVC3 = createTabController(title: "Jobs", image: .job, index: 4)
        let controllers: [UINavigationController] = [homeVC, dummyVC1, createPostVC, dummyVC2, dummyVC3]
        
        prepareTabControllers(withTabControllers: controllers)
    }
    
    func prepareTabControllers(withTabControllers tabControllers: [UIViewController]) {
        tabBarController.delegate = self
        tabBarController.setViewControllers(tabControllers, animated: true)
        tabBarController.selectedIndex = 0
        navigationController.viewControllers = [tabBarController]
    }
    
    func createTabController(_ vc: UIViewController = UIViewController(), title: String, image: UIImage?, index: Int) -> UINavigationController {
        vc.view.backgroundColor = .appLightBlue
        
        let navController = UINavigationController()
            navController.tabBarItem = UITabBarItem(title: title, image: image, tag: index)
            navController.setNavigationBarHidden(true, animated: true)
            navController.pushViewController(vc, animated: true)
        
        return navController
    }
    
    func hideTabBar() {
        self.tabBarController.tabBar.isHidden = true
    }
    
    func showTabBar() {
        self.tabBarController.tabBar.isHidden = false
    }
    
    func getHomeViewController() -> HomeViewController { self.homeViewController }
    func getCreatePostViewController() -> CreatePostViewController { self.createPostViewController }
}

extension TabBarCoordinator: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("tabBar is selected")
    }
}
