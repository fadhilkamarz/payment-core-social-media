//
//  Coordinator.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 02/09/21.
//

import UIKit

protocol Coordinator: AnyObject {
    var childCoordinators: [Coordinator] { get set }
    
    var navigationController: UINavigationController { get set }
    
    var finishDelegate: CoordinatorFinishDelegate? { get set }
    
    func start()
    func finish()
    
    init(_ navigationController: UINavigationController)
}

extension Coordinator {
    func finish() {
        childCoordinators.removeAll()
        finishDelegate?.coordinatorDidFinish(childCoordinator: self)
    }
}

protocol CoordinatorFinishDelegate: AnyObject {
    func coordinatorDidFinish(childCoordinator: Coordinator)
}

final class AppCoordinator: Coordinator {
    var childCoordinators: [Coordinator]
    
    var navigationController: UINavigationController

    weak var finishDelegate: CoordinatorFinishDelegate?
    
    var tabBarCoordinator: TabBarCoordinator
    
    var homeVC: HomeViewController?
    var createPostVC: CreatePostViewController?
    
    let homeCoordinator: HomeCoordinator
    let createPostCoordinator: CreatePostCoordinator
    
    required init(_ navigationController: UINavigationController) {
        self.childCoordinators = []
        self.homeCoordinator = HomeCoordinator(navigationController)
        self.createPostCoordinator = CreatePostCoordinator(navigationController)
        self.navigationController = navigationController
        self.navigationController.setNavigationBarHidden(true, animated: true)
        self.tabBarCoordinator = TabBarCoordinator(navigationController)
    }
    
    func configureHomeCoordinator() {
        homeCoordinator.vc = homeVC
        homeCoordinator.parent = tabBarCoordinator
        homeCoordinator.start()
    }
    
    func configureCreatePostCoordinator() {
        createPostCoordinator.vc = createPostVC
        createPostCoordinator.parent = tabBarCoordinator
        createPostCoordinator.start()
    }
    
    func start() {
        tabBarCoordinator.finishDelegate = self
        tabBarCoordinator.start()
        
        homeVC = tabBarCoordinator.getHomeViewController()
        configureHomeCoordinator()
        
        createPostVC = tabBarCoordinator.getCreatePostViewController()
        configureCreatePostCoordinator()
        
        childCoordinators.append(tabBarCoordinator)
    }
    
}

extension AppCoordinator: CoordinatorFinishDelegate {
    func coordinatorDidFinish(childCoordinator: Coordinator) {
        navigationController.viewControllers.removeAll()
        start()
    }
}

final class HomeCoordinator: Coordinator {
    var childCoordinators: [Coordinator]
    
    var navigationController: UINavigationController
    
    var finishDelegate: CoordinatorFinishDelegate?
    
    weak var parent: TabBarCoordinator?
    
    var vc: HomeViewController?
    
    required init(_ navigationController: UINavigationController) {
        self.childCoordinators = []
        self.navigationController = navigationController
    }
    
    func start() {
        guard let homeVC = self.vc else { return }
        homeVC.viewModel.coordinator = self
    }
    
    func goToPostVC(_ post: Post) {
        let postCoordinator = PostCoordinator(navigationController)
            postCoordinator.parent = self
            postCoordinator.post = post
            postCoordinator.start()
        childCoordinators.append(postCoordinator)
    }
    
    func hideNavigationBar() {
        self.navigationController.setNavigationBarHidden(true, animated: true)
    }
    
    func showTabBar() {
        parent?.showTabBar()
    }
    
}

final class PostCoordinator: Coordinator {
    var childCoordinators: [Coordinator]
    
    var navigationController: UINavigationController
    
    var finishDelegate: CoordinatorFinishDelegate?
    
    weak var parent: HomeCoordinator?
    
    var post: Post?
    
    func start() {
        guard let post = self.post else { return }
        
        let postVC = PostViewController(post: post)
            postVC.viewModel.coordinator = self
        
        navigationController.pushViewController(postVC, animated: true)
    }
    
    required init(_ navigationController: UINavigationController) {
        self.childCoordinators = []
        self.navigationController = navigationController
        self.navigationController.setNavigationBarHidden(true, animated: true)
    }
    
}

final class CreatePostCoordinator: Coordinator {
    var childCoordinators: [Coordinator]
    
    var navigationController: UINavigationController
    
    var finishDelegate: CoordinatorFinishDelegate?
    
    weak var parent: TabBarCoordinator?
    
    var vc: CreatePostViewController?
        
    required init(_ navigationController: UINavigationController) {
        self.childCoordinators = []
        self.navigationController = navigationController
        self.navigationController.setNavigationBarHidden(true, animated: true)
    }
    
    func start() {
        guard let createPostVC = self.vc else { return }
        createPostVC.viewModel.coordinator = self
    }
    
    func hideTabBar() {
        parent?.hideTabBar()
    }
    
    func goBackToHome() {
        parent?.finish()
    }
    
    func updatePost(with newPost: Post) {
        Post.addPost(with: newPost)
        parent?.finish()
    }
    
}
