//
//  PostViewController.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 02/09/21.
//

import UIKit
import AsyncDisplayKit

final class PostViewController: ASDKViewController<ASDisplayNode> {
    
    // MARK: - Nodes
    private let postDetailNode: PostDetailNode
    
    // MARK: - View Model
    let viewModel: PostViewModel
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        if Device.NavigationBar.height.isZero {
            Device.NavigationBar.height = self.navigationController?.navigationBar.bounds.height ?? 0
        }
    }
    
    init(post: Post) {
        self.viewModel = PostViewModel()
        self.postDetailNode = PostDetailNode(delegate: viewModel, post: post)
        
        super.init(node: ASDisplayNode())
        
        self.node.backgroundColor = .appLightBlue
        self.node.automaticallyManagesSubnodes = true
        self.node.layoutSpecBlock = { _, _ in
            
            let mainStack = ASStackLayoutSpec(direction: .vertical,
                                              spacing: 8,
                                              justifyContent: .start,
                                              alignItems: .stretch,
                                              children: [self.postDetailNode])
            
            return mainStack
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
