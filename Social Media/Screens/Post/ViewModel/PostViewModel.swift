//
//  PostViewModel.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 02/09/21.
//

import UIKit

final class PostViewModel: NSObject {
    // MARK: - Coordinator
    var coordinator: PostCoordinator?
    
}

extension PostViewModel: ActionNodeDelegate {
    func didTapLike(at post: Post) {
        
    }
    
    func didTapComment() {
        
    }
    
    func didTapShare() { }
}
