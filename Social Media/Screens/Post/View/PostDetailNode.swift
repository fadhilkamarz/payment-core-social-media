//
//  PostDetailNode.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 03/09/21.
//

import AsyncDisplayKit

final class PostDetailNode: ASDisplayNode {
    
    // MARK: - Nodes
    private let userNode: UserNode
    private let contentNode: ContentNode
    private let actionNode: ActionNode
    private let likesNode: LikesNode
    private let commentNode: CommentNode
    
    init(delegate: PostViewModel, post: Post) {
        userNode = UserNode(post: post)
        contentNode = ContentNode(post: post)
        actionNode = ActionNode(delegate: delegate, post: post)
        likesNode = LikesNode(likes: post.likes)
        commentNode = CommentNode(delegate: delegate, comment: post.comments[0])
        
        super.init()
        
        self.automaticallyManagesSubnodes = true
        self.automaticallyRelayoutOnSafeAreaChanges = true
        self.backgroundColor = .white
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let likesTitle = ASTextNode()
            likesTitle.attributedText = NSAttributedString(string: "Likes", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 18)])
            likesTitle.style.height = ASDimension(unit: .points, value: 28)
            likesTitle.textContainerInset = UIEdgeInsets(top: 8, left: 8, bottom: 0, right: 0)
        
        let commentTitle = ASTextNode()
            commentTitle.attributedText = NSAttributedString(string: "Comments", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 18)])
            commentTitle.style.height = ASDimension(unit: .points, value: 28)
            commentTitle.textContainerInset = UIEdgeInsets(top: 8, left: 8, bottom: 0, right: 0)
        
        let mainStack = ASStackLayoutSpec(direction: .vertical,
                                          spacing: 8,
                                          justifyContent: .start,
                                          alignItems: .start,
                                          children: [userNode, contentNode, actionNode, likesTitle, likesNode, commentTitle, commentNode])
        
        let layout = ASInsetLayoutSpec(insets: UIEdgeInsets(top:  Device.SafeArea.top+Device.NavigationBar.height+8, left: 8, bottom: 8, right: 8), child: mainStack)
        return layout
    }
}
