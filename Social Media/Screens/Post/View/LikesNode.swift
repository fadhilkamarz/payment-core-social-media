//
//  LikesNode.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 03/09/21.
//

import AsyncDisplayKit
 
class LikesNode: ASCollectionNode {
    let likes: [User]
    
    init(likes: [User]) {
        self.likes = likes
        
        let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 8
            layout.minimumInteritemSpacing = 0
        
        super.init(frame: .zero, collectionViewLayout: layout, layoutFacilitator: nil)
        
        self.style.width = ASDimension(unit: .fraction, value: 1)
        self.style.height = ASDimension(unit: .points, value: 75)
 
        self.delegate = self
        self.dataSource = self
        
        self.view.showsHorizontalScrollIndicator = false
    }
}
 
extension LikesNode: ASCollectionDataSource, ASCollectionDelegate {
    func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
        return 1
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        return likes.count
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard likes.count > indexPath.row else { return { ASCellNode() } }
        
        let user = likes[indexPath.row]
        
        let cellNodeBlock = { () -> ASCellNode in
            return LikesCell(user: user)
        }
        
        return cellNodeBlock
    }
}
