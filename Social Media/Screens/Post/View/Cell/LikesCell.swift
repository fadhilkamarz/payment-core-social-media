//
//  LikesCell.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 03/09/21.
//

import AsyncDisplayKit
 
final class LikesCell: ASCellNode {
    private let user: User
    
    // MARK: - Nodes
    private let userPicture: ASImageNode
    private let userName: ASTextNode
    
    init(user: User) {
        self.user = user
        
        userPicture = ASImageNode()
        userPicture.image = user.photo
        userPicture.style.preferredSize = CGSize(width: 48, height: 48)
        userPicture.cornerRadius = 48/2
        
        userName = ASTextNode()
        userName.attributedText = NSAttributedString.subtitle(user.name)
        userName.maximumNumberOfLines = 1
        
        super.init()
        
        self.automaticallyManagesSubnodes = true
        
        self.style.width = ASDimension(unit: .points, value: 55)
        self.style.height = ASDimension(unit: .fraction, value: 1)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let mainStack = ASStackLayoutSpec(direction: .vertical,
                                          spacing: 8,
                                          justifyContent: .center,
                                          alignItems: .center,
                                          children: [userPicture, userName])
        
        return mainStack
    }
}
