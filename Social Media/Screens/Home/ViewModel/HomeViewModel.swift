//
//  HomeViewModel.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 02/09/21.
//

import UIKit

final class HomeViewModel: NSObject {
    // MARK: - Coordinator
    var coordinator: HomeCoordinator?
    
    // MARK: - Closure
    var onTapUpdateLikes: (() -> Void)?
    
}

extension HomeViewModel: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print(searchBar.text ?? "\(#function) with no text")
    }
}

extension HomeViewModel: ActionNodeDelegate {
    func didTapLike(at post: Post) {
        Post.addLike(from: post.user, for: post)
        onTapUpdateLikes?()
    }
    
    func didTapComment() {
        
    }
    
    func didTapShare() {
        
    }
}
