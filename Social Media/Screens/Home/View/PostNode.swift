//
//  PostNode.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 02/09/21.
//

import AsyncDisplayKit
import RxRelay
import RxSwift

final class PostNode: ASTableNode {
    
    var posts: [Post] = []
    
    let viewModel: HomeViewModel
    
    private let disposeBag = DisposeBag()
    
    init(delegate: HomeViewModel, posts: [Post]) {
        self.viewModel = delegate
        self.posts = posts
        
        super.init(style: .plain)
        
        self.delegate = self
        self.dataSource = self
        
        self.view.separatorStyle = .none
        
        self.style.width = ASDimension(unit: .fraction, value: 1)
        self.style.height = ASDimension(unit: .fraction, value: 1)
        self.style.flexShrink = 1
        self.backgroundColor = .appLightBlue
        
        setupPostObserver()
    }
    
    func setupPostObserver() {
        Post.dummyPostsRelay.asObservable()
            .subscribe(onNext: { [unowned self] newPost in
                self.posts = newPost
                self.reloadData()
            }).disposed(by: disposeBag)
    }
    
}

extension PostNode: ASTableDataSource, ASTableDelegate {
    func numberOfSections(in tableNode: ASTableNode) -> Int {
        return posts.count
    }
    
    func tableNode(_ tableNode: ASTableNode, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableNode(_ tableNode: ASTableNode, nodeBlockForRowAt indexPath: IndexPath) -> ASCellNodeBlock {
        guard posts.count > indexPath.row else { return { ASCellNode() } }
        
        let post = posts[indexPath.row]
        
        let cellNodeBlock = { () -> ASCellNode in
            return PostCell(delegate: self.viewModel, post: post)
        }
        
        return cellNodeBlock
    }
    
    func tableNode(_ tableNode: ASTableNode, didSelectRowAt indexPath: IndexPath) {
        tableNode.deselectRow(at: indexPath, animated: true)
        viewModel.coordinator?.goToPostVC(posts[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat { 4 }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let bottomSeparatorLine = UIView(frame: CGRect(x: 0, y: 0, width: Device.Screen.width, height: 4))
            bottomSeparatorLine.backgroundColor = .appLightBlue
        return bottomSeparatorLine
    }
}
