//
//  ContentNode.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 02/09/21.
//

import AsyncDisplayKit

final class ContentNode: ASDisplayNode {
    
    private let postContent: ASTextNode
    private let postTotalLikes: ASTextNode
    private let postTotalComments: ASTextNode
    
    private let post: Post
    
    init(post: Post, maximumNumberOfLines: UInt = 0) {
        self.post = post
        
        postContent = ASTextNode()
        postContent.maximumNumberOfLines = maximumNumberOfLines
        postContent.truncationMode = .byTruncatingTail
        postContent.truncationAttributedText = NSAttributedString(string: "...")
        postContent.attributedText = NSAttributedString(string: post.content, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)])
        
        let totalLikes = post.likes.count > 1 ? "\(post.likes.count) Likes  \u{2022}" : "\(post.likes.count) Like  \u{2022}"
        postTotalLikes = ASTextNode()
        postTotalLikes.attributedText = NSAttributedString(string: totalLikes, attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        let totalComments = post.comments.count > 1 ? "\(post.comments.count) Comments" : "\(post.comments.count) Comment"
        postTotalComments = ASTextNode()
        postTotalComments.attributedText = NSAttributedString(string: totalComments, attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        super.init()
        
        self.automaticallyManagesSubnodes = true
        self.automaticallyRelayoutOnSafeAreaChanges = true
        self.backgroundColor = .white
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let bottomStackComponents: [ASLayoutElement] = [postTotalLikes, postTotalComments]
        
        let bottomStack = ASStackLayoutSpec(direction: .horizontal,
                                          spacing: 4,
                                          justifyContent: .start,
                                          alignItems: .center,
                                          children: bottomStackComponents)
        
        let mainStack = ASStackLayoutSpec(direction: .vertical,
                                          spacing: 8,
                                          justifyContent: .start,
                                          alignItems: .start,
                                          children: [postContent, bottomStack])
        
        let layout = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 8, left: 8, bottom: 0, right: 8), child: mainStack)
        
        return layout
    }
}
