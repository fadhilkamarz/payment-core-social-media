//
//  UserNode.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 02/09/21.
//

import AsyncDisplayKit

final class UserNode: ASDisplayNode {
    
    private let userPicture: ASImageNode
    private let userName: ASTextNode
    private let userPosition: ASTextNode
    private let date: ASTextNode
    
    private let post: Post
    
    init(post: Post) {
        self.post = post
        
        userPicture = ASImageNode()
        userPicture.image = post.user.photo
        userPicture.style.preferredSize = CGSize(width: 56, height: 56)
        userPicture.cornerRadius = 56/2
        
        userName = ASTextNode()
        userName.attributedText = NSAttributedString(string: post.user.name, attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16)])
        
        userPosition = ASTextNode()
        userPosition.attributedText = NSAttributedString(string: post.user.position, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        date = ASTextNode()
        date.attributedText = NSAttributedString(string: post.date, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        super.init()
        
        self.automaticallyManagesSubnodes = true
        self.automaticallyRelayoutOnSafeAreaChanges = true
        self.backgroundColor = .white
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let trailingStackComponents: [ASLayoutElement] = [userName, userPosition, date]
        
        let trailingStack = ASStackLayoutSpec(direction: .vertical,
                                          spacing: 4,
                                          justifyContent: .start,
                                          alignItems: .start,
                                          children: trailingStackComponents)
        
        let mainStack = ASStackLayoutSpec(direction: .horizontal,
                                          spacing: 8,
                                          justifyContent: .start,
                                          alignItems: .center,
                                          children: [userPicture, trailingStack])
        
        let layout = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8), child: mainStack)
        
        return layout
    }
}
