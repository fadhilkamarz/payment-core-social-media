//
//  CommentNode.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 02/09/21.
//

import AsyncDisplayKit

final class CommentNode: ASDisplayNode {
    
    private let userPicture: ASImageNode
    private let userName: ASTextNode
    private let userPosition: ASTextNode
    private let userComment: ASTextNode
    private let likeButton: ASButtonNode
    private let commentButton: ASButtonNode
    
    private var delegate: ActionNodeDelegate?
    
    init(delegate: ActionNodeDelegate, comment: Comment) {
        self.delegate = delegate
        
        userPicture = ASImageNode()
        userPicture.image = comment.user.photo
        userPicture.style.preferredSize = CGSize(width: 48, height: 48)
        userPicture.cornerRadius = 48/2
        
        userName = ASTextNode()
        userName.attributedText = NSAttributedString(string: comment.user.name, attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16)])
        
        userPosition = ASTextNode()
        userPosition.attributedText = NSAttributedString(string: comment.user.position, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        userComment = ASTextNode()
        userComment.style.preferredSize = CGSize(width: Device.Screen.width-90, height: 40)
        userComment.truncationMode = .byTruncatingTail
        userComment.truncationAttributedText = NSAttributedString(string: "...")
        userComment.attributedText = NSAttributedString(string: comment.content, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)])
        
        likeButton = ASButtonNode()
        likeButton.style.preferredSize = CGSize(width: 16, height: 16)
        likeButton.imageNode.contentMode = .scaleAspectFit
        likeButton.setImage(.like, for: .normal)
        
        commentButton = ASButtonNode()
        commentButton.style.preferredSize = CGSize(width: 16, height: 16)
        commentButton.imageNode.contentMode = .scaleAspectFit
        commentButton.setImage(.comment, for: .normal)
        
        super.init()
        
        likeButton.addTarget(self, action: #selector(didTapLike(_:)), forControlEvents: .touchUpInside)
        commentButton.addTarget(self, action: #selector(didTapComment(_:)), forControlEvents: .touchUpInside)
        
        self.automaticallyManagesSubnodes = true
        self.automaticallyRelayoutOnSafeAreaChanges = true
        self.backgroundColor = .white
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let actionComponents = ASStackLayoutSpec(direction: .horizontal,
                                                 spacing: 16,
                                                 justifyContent: .start,
                                                 alignItems: .center,
                                                 children: [likeButton, commentButton])
        
        let trailingStack = ASStackLayoutSpec(direction: .vertical,
                                          spacing: 6,
                                          justifyContent: .start,
                                          alignItems: .start,
                                          children: [userName, userPosition, userComment, actionComponents])
        
        let mainStack = ASStackLayoutSpec(direction: .horizontal,
                                          spacing: 8,
                                          justifyContent: .start,
                                          alignItems: .start,
                                          children: [userPicture, trailingStack])
        
        let layout = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 8, left: 8, bottom: 0, right: 8), child: mainStack)
        
        return layout
    }
    
    @objc func didTapLike(_ sender: ASButtonNode) {
//        delegate?.didTapLike(at: post)
    }
    
    @objc func didTapComment(_ sender: ASButtonNode) {
        delegate?.didTapComment()
    }
}
