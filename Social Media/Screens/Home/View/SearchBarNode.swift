//
//  SearchBarNode.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 01/09/21.
//

import AsyncDisplayKit

final class SearchBarNode: ASDisplayNode {
    
    var searchBar: UISearchBar? {
        return self.view as? UISearchBar
    }

    weak var delegate: UISearchBarDelegate?
    
    init(delegate: UISearchBarDelegate?, height: CGFloat = 44) {
        self.delegate = delegate
        
        super.init()
        
        self.setViewBlock {
            UISearchBar()
        }
        
        style.preferredSize = CGSize(width: UIScreen.main.bounds.width-112, height: height)
    }
    
    override func didLoad() {
        super.didLoad()
        guard let searchBar = self.searchBar else { return }
        searchBar.delegate = delegate
        searchBar.searchBarStyle = .minimal
        searchBar.tintColor = .black
        searchBar.backgroundColor = .clear
        searchBar.placeholder = "Search"
    }
}
