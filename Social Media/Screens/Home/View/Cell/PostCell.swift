//
//  PostCell.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 03/09/21.
//

import AsyncDisplayKit
 
final class PostCell: ASCellNode {
    
    // MARK: - Nodes
    private let userNode: UserNode
    private let contentNode: ContentNode
    private let actionNode: ActionNode
    private let commentNode: CommentNode
    
    private let post: Post
    
    init(delegate: HomeViewModel, post: Post) {
        self.post = post
        
        userNode = UserNode(post: post)
        contentNode = ContentNode(post: post, maximumNumberOfLines: 5)
        actionNode = ActionNode(delegate: delegate, post: post)
        commentNode = CommentNode(delegate: delegate, comment: Post.dummyPostsRelay.value[0].comments[0])
        
        super.init()
        
        self.automaticallyManagesSubnodes = true
        self.automaticallyRelayoutOnSafeAreaChanges = true
        self.backgroundColor = .white
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let mainStack = ASStackLayoutSpec(direction: .vertical,
                                          spacing: 8.0,
                                          justifyContent: .start,
                                          alignItems: .start,
                                          children: [userNode, contentNode, actionNode, commentNode])
        
        let layout = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8), child: mainStack)
        return layout
    }
}
