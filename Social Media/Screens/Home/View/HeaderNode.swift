//
//  HeaderNode.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 01/09/21.
//

import AsyncDisplayKit

final class HeaderNode: ASDisplayNode {
        
    private let profilePicture: ASImageNode
    private let searchNode: SearchBarNode
    private let messsageButton: ASButtonNode
    
    private let user: User

    init(delegate: HomeViewModel, user: User) {
        self.user = user
        
        profilePicture = ASImageNode()
        profilePicture.image = user.photo
        profilePicture.style.preferredSize = CGSize(width: 32, height: 32)
        profilePicture.cornerRadius = 32/2
        
        searchNode = SearchBarNode(delegate: delegate)
        
        messsageButton = ASButtonNode()
        messsageButton.style.preferredSize = CGSize(width: 32, height: 32)
        messsageButton.backgroundImageNode.contentMode = .scaleAspectFit
        messsageButton.setBackgroundImage(.message, for: .normal)
        
        super.init()
        
        self.automaticallyManagesSubnodes = true
        self.automaticallyRelayoutOnSafeAreaChanges = true
        self.backgroundColor = .white
    }

    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let mainStack = ASStackLayoutSpec(direction: .horizontal,
                                          spacing: 8.0,
                                          justifyContent: .start,
                                          alignItems: .center,
                                          children: [profilePicture, searchNode, messsageButton])
        
        let topPadding = Device.SafeArea.top + 8
        let layout = ASInsetLayoutSpec(insets: UIEdgeInsets(top: topPadding, left: 16, bottom: 8, right: 8), child: mainStack)
        
        return layout
    }
}
