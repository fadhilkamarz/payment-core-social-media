//
//  ActionNode.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 02/09/21.
//

import AsyncDisplayKit

protocol ActionNodeDelegate: AnyObject {
    func didTapLike(at post: Post)
    func didTapComment()
    func didTapShare()
}

final class ActionNode: ASDisplayNode {
    
    private let likeButton: ASButtonNode
    private let commentButton: ASButtonNode
    private let shareButton: ASButtonNode
    
    weak var delegate: ActionNodeDelegate?
    
    let post: Post
    
    init(delegate: ActionNodeDelegate, post: Post) {
        self.post = post
        self.delegate = delegate
        
        likeButton = ASButtonNode()
        likeButton.style.flexGrow = 1
        likeButton.setTitle("Like", with: .boldSystemFont(ofSize: 14), with: .darkGray, for: .normal)
        likeButton.imageNode.contentMode = .scaleAspectFit
        likeButton.setImage(.like, for: .normal)
        
        
        commentButton = ASButtonNode()
        commentButton.style.flexGrow = 1
        commentButton.setTitle("Comment", with: .boldSystemFont(ofSize: 14), with: .darkGray, for: .normal)
        commentButton.imageNode.contentMode = .scaleAspectFit
        commentButton.setImage(.comment, for: .normal)

        shareButton = ASButtonNode()
        shareButton.style.flexGrow = 1
        shareButton.setTitle("Share", with: .boldSystemFont(ofSize: 14), with: .darkGray, for: .normal)
        shareButton.imageNode.contentMode = .scaleAspectFit
        shareButton.setImage(.share, for: .normal)
        
        super.init()

        likeButton.addTarget(self, action: #selector(didTapLike(_:)), forControlEvents: .touchUpInside)
        commentButton.addTarget(self, action: #selector(didTapComment(_:)), forControlEvents: .touchUpInside)
        shareButton.addTarget(self, action: #selector(didTapShare(_:)), forControlEvents: .touchUpInside)
        
        self.automaticallyManagesSubnodes = true
        self.automaticallyRelayoutOnSafeAreaChanges = true
        self.backgroundColor = .white
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        self.style.preferredSize = CGSize(width: Device.Screen.width, height: 40)
        
        let actionStack = ASStackLayoutSpec(direction: .horizontal,
                                          spacing: 0,
                                          justifyContent: .center,
                                          alignItems: .stretch,
                                          children: [likeButton, commentButton, shareButton])
        
        let topSeparatorLine = ASDisplayNode()
            topSeparatorLine.style.preferredSize = CGSize(width: Device.Screen.width-32, height: 1)
            topSeparatorLine.backgroundColor = UIColor.lightGray.withAlphaComponent(0.6)
        let bottomSeparatorLine = ASDisplayNode()
            bottomSeparatorLine.style.preferredSize = CGSize(width: Device.Screen.width-32, height: 1)
            bottomSeparatorLine.backgroundColor = UIColor.lightGray.withAlphaComponent(0.6)
        
        let mainStack = ASStackLayoutSpec(direction: .vertical,
                                          spacing: 8,
                                          justifyContent: .start,
                                          alignItems: .stretch,
                                          children: [topSeparatorLine, actionStack, bottomSeparatorLine])
        
        let layout = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 8, left: 8, bottom: 0, right: 8), child: mainStack)
        
        return layout
    }
    
    @objc func didTapLike(_ sender: ASButtonNode) {
        delegate?.didTapLike(at: post)
    }
    
    @objc func didTapComment(_ sender: ASButtonNode) {
        delegate?.didTapComment()
    }
    
    @objc func didTapShare(_ sender: ASButtonNode) {
        delegate?.didTapShare()
    }
}
