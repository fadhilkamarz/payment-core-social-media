//
//  HomeViewController.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 01/09/21.
//

import UIKit
import AsyncDisplayKit

final class HomeViewController: ASDKViewController<ASDisplayNode> {
    // MARK: - Nodes
    private let headerNode: HeaderNode
    private let postNode: PostNode

    // MARK: - View Model
    let viewModel: HomeViewModel

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        postNode.reloadData()
        viewModel.coordinator?.hideNavigationBar()
        viewModel.coordinator?.showTabBar()
    }

    override init() {
        self.viewModel = HomeViewModel()
        self.headerNode = HeaderNode(delegate: self.viewModel, user: User.user1)
        self.postNode = PostNode(delegate: self.viewModel, posts: Post.dummyPostsRelay.value)

        super.init(node: ASDisplayNode())

        self.node.backgroundColor = .appLightBlue
        self.node.automaticallyManagesSubnodes = true
        self.node.layoutSpecBlock = { _, _ in

            let mainStack = ASStackLayoutSpec(direction: .vertical,
                                              spacing: 4,
                                              justifyContent: .start,
                                              alignItems: .stretch,
                                              children: [self.headerNode, self.postNode])

            return mainStack
        }

        self.viewModel.onTapUpdateLikes = { [weak self] in
            self?.postNode.reloadData()
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
