//
//  CreatePostViewController.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 03/09/21.
//

import UIKit
import AsyncDisplayKit

final class CreatePostViewController: ASDKViewController<ASDisplayNode> {
    // MARK: - Nodes
    private let createPostNode: CreatePostNode
    private let profilePicture: ASImageNode
    private let createPostTextNode: ASEditableTextNode
    
    // MARK: - View Model
    let viewModel: CreatePostViewModel
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        createPostTextNode.textView.text = ""
        viewModel.resetPost()
        viewModel.coordinator?.hideTabBar()
    }
    
    override init() {
        self.viewModel = CreatePostViewModel(user: User.user1)
        self.createPostNode = CreatePostNode(delegate: self.viewModel, user: User.user1)
        
        profilePicture = ASImageNode()
        profilePicture.backgroundColor = .white
        profilePicture.image = User.user1.photo
        profilePicture.style.preferredSize = CGSize(width: 56, height: 56)
        profilePicture.cornerRadius = 56/2
        profilePicture.style.alignSelf = .center
        
        createPostTextNode = ASEditableTextNode()
        createPostTextNode.backgroundColor = .white
        createPostTextNode.attributedPlaceholderText = NSAttributedString(string: "Create post...", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)])
        createPostTextNode.textContainerInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        createPostTextNode.style.preferredSize = CGSize(width: Device.Screen.width-16, height: Device.Screen.height-Device.SafeArea.top-56)
        createPostTextNode.style.alignSelf = .center
        createPostTextNode.textView.font = .systemFont(ofSize: 16)
        createPostTextNode.textView.autocorrectionType = .no
        
        super.init(node: ASDisplayNode())
        
        createPostTextNode.delegate = viewModel
        
        self.node.backgroundColor = .appLightBlue
        self.node.automaticallyManagesSubnodes = true
        self.node.layoutSpecBlock = { _, _ in
            
            let mainStack = ASStackLayoutSpec(direction: .vertical,
                                              spacing: 8,
                                              justifyContent: .start,
                                              alignItems: .stretch,
                                              children: [self.createPostNode, self.profilePicture, self.createPostTextNode])
            
            return mainStack
        }
        
        viewModel.onClose = { [weak self] in
            self?.viewModel.coordinator?.goBackToHome()
        }
        
        viewModel.onCompleteCreatePost = { [weak self] in
            guard let newPost = self?.viewModel.newPost else { return }
            self?.viewModel.coordinator?.updatePost(with: newPost)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
