//
//  CreatePostViewModel.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 03/09/21.
//

import UIKit
import AsyncDisplayKit

final class CreatePostViewModel: NSObject {
    // MARK: - Coordinator
    var coordinator: CreatePostCoordinator?
    
    // MARK: - Closure
    var onClose: (() -> Void)?
    var onCompleteCreatePost: (() -> Void)?
    
    var newPost: Post
    var user: User
    
    init(user: User) {
        let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "d MMMM yyyy"
        let currentDate = dateFormatter.string(from: Date())
        
        self.newPost = Post(postID: 0, user: user, date: currentDate, content: "", comments: [], likes: [])
        self.user = user
    }
    
    func resetPost() {
        let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "d MMMM yyyy"
        let currentDate = dateFormatter.string(from: Date())
        
        self.newPost = Post(postID: 0, user: user, date: currentDate, content: "", comments: [], likes: [])
    }
    
}

extension CreatePostViewModel: ASEditableTextNodeDelegate {
    func editableTextNodeDidUpdateText(_ editableTextNode: ASEditableTextNode) {
        newPost.content = editableTextNode.textView.text
    }
    
    func editableTextNodeDidFinishEditing(_ editableTextNode: ASEditableTextNode) {
        newPost.content = editableTextNode.textView.text
    }
}
