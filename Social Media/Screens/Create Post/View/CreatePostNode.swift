//
//  CreatePostNode.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 03/09/21.
//

import AsyncDisplayKit

final class CreatePostNode: ASDisplayNode {
        
    private let closeButton: ASButtonNode
    private let postButton: ASButtonNode
    
    private let user: User
    
    weak var delegate: CreatePostViewModel?

    init(delegate: CreatePostViewModel, user: User) {
        self.delegate = delegate
        self.user = user
        
        closeButton = ASButtonNode()
        closeButton.style.preferredSize = CGSize(width: 24, height: 24)
        closeButton.backgroundImageNode.contentMode = .scaleAspectFit
        closeButton.setBackgroundImage(.close, for: .normal)
        
        postButton = ASButtonNode()
        postButton.style.preferredSize = CGSize(width: 64, height: 32)
        postButton.setTitle("Post", with: .systemFont(ofSize: 16), with: .black, for: .normal)
        
        super.init()
        
        closeButton.addTarget(self, action: #selector(didTapClose(_:)), forControlEvents: .touchUpInside)
        postButton.addTarget(self, action: #selector(didTapPost(_:)), forControlEvents: .touchUpInside)
        
        self.automaticallyManagesSubnodes = true
        self.automaticallyRelayoutOnSafeAreaChanges = true
        self.backgroundColor = .white
        self.style.width = ASDimension(unit: .points, value: Device.Screen.width)
    }

    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let mainStack = ASStackLayoutSpec(direction: .horizontal,
                                          spacing: 8.0,
                                          justifyContent: .spaceBetween,
                                          alignItems: .center,
                                          children: [closeButton, postButton])
        
        let topPadding = Device.SafeArea.top + 8
        let layout = ASInsetLayoutSpec(insets: UIEdgeInsets(top: topPadding, left: 16, bottom: 8, right: 8), child: mainStack)
        
        return layout
    }
    
    @objc func didTapClose(_ sender: ASButtonNode) {
        delegate?.onClose?()
    }
    
    @objc func didTapPost(_ sender: ASButtonNode) {
        delegate?.onCompleteCreatePost?()
    }
}

