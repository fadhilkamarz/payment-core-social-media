//
//  Post.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 02/09/21.
//

import Foundation
import RxRelay

struct Post {
    var postID: Int
    let user: User
    var date: String
    var content: String
    var comments: [Comment]
    var likes: [User]
    
    static var dummyPostsRelay: BehaviorRelay<[Post]> = BehaviorRelay(value: [
        Post(postID: 1,
             user: User.user1,
             date: "1 September 2021",
             content: "User 1 post 1 \nSocial Media[45550:1096773] [Node] Propagating new traits for <Social_Media.HomeViewController: 0x7ff7e1407940>: { verticalSizeClass = Regular; horizontalSizeClass = Compact; displayScale = 3; userInterfaceIdiom = Phone; forceTouchCapability = Unavailable; userInterfaceStyle = Light; layoutDirection = LeftToRight; preferredContentSizeCategory = UICTContentSizeCategoryL; displayGamut = P3; userInterfaceLevel = Base; accessibilityContrast = Normal; legibilityWeight = Regular; containerSize = {428, 926} }",
             comments: Comment.dummyComments,
             likes: [User.user2])
    ])
    
    static func addLike(from user: User, for post: Post) {
        let newValue = Post.dummyPostsRelay.value.compactMap { dummyPost -> Post in
            if post.postID == dummyPost.postID {
                return Post(postID: dummyPost.postID, user: dummyPost.user, date: dummyPost.date, content: dummyPost.content, comments: dummyPost.comments, likes: dummyPost.likes+[user])
            }
            return dummyPost
        }
        Post.dummyPostsRelay.accept(newValue)
    }
    
    static func addPost(with newPost: Post) {
        let newValue = Post.dummyPostsRelay.value + [newPost]
        Post.dummyPostsRelay.accept(newValue)
    }
}
