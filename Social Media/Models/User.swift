//
//  User.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 01/09/21.
//

import UIKit

struct User {
    let name: String
    let photo: UIImage?
    let position: String
    
    static var user1: User {
        return User(name: "Bakugo", photo: UIImage(systemName: "person.circle"), position: "iOS Engineer")
    }
    
    static var user2: User {
        return User(name: "Deku", photo: UIImage(systemName: "person.circle"), position: "Senior iOS Engineer")
    }
}
