//
//  Comment.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 02/09/21.
//

import Foundation

struct Comment {
    let user: User
    let content: String
    
    static var dummyComments: [Comment] = [
        Comment(user: User.user2, content: "Hello World \nSocial Media[45550:1096773] [Node] Propagating new traits for <Social_Media.HomeViewController: 0x7ff7e1407940>: { verticalSizeClass = Regular; horizontalSizeClass = Compact; displayScale = 3; userInterfaceIdiom = Phone; forceTouchCapability = Unavailable; userInterfaceStyle = Light; layoutDirection = LeftToRight; preferredContentSizeCategory = UICTContentSizeCategoryL; displayGamut = P3; userInterfaceLevel = Base; accessibilityContrast = Normal; legibilityWeight = Regular; containerSize = {428, 926} }")
    ]
}
