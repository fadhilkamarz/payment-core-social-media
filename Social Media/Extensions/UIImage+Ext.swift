//
//  UIImage+Ext.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 02/09/21.
//

import UIKit

extension UIImage {
    static let addPost = UIImage(systemName: "plus.circle")
    static let close = UIImage(systemName: "xmark")
    static let comment = UIImage(systemName: "text.bubble")
    static let home = UIImage(systemName: "house")
    static let job = UIImage(systemName: "briefcase")
    static let like = UIImage(systemName: "hand.thumbsup")
    static let message = UIImage(systemName: "bubble.left.and.bubble.right")
    static let notification = UIImage(systemName: "bell")
    static let people = UIImage(systemName: "person.2")
    static let share = UIImage(systemName: "arrowshape.turn.up.right")
}
