//
//  UIColor+Ext.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 01/09/21.
//

import UIKit

extension UIColor {
    static let appBlue: UIColor = UIColor(red: 0.18, green: 0.55, blue: 0.90, alpha: 1.00)
    static let appLightBlue: UIColor = UIColor(red: 0.87, green: 0.92, blue: 0.98, alpha: 1.00)
}
