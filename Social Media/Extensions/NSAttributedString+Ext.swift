//
//  NSAttributedString+Ext.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 03/09/21.
//

import UIKit

extension NSAttributedString {
    static func subtitle(_ text: String) -> NSAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont.systemFont(ofSize: 10, weight: .light)]
        let string = NSAttributedString(string:text, attributes: attrs)
        
        return string
    }
}
