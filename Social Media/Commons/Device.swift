//
//  Device.swift
//  Social Media
//
//  Created by Fadhil Hanri Kamarz on 01/09/21.
//

import UIKit

struct Device {
    struct Screen {
        static var height: CGFloat = 0
        static var width: CGFloat = 0
    }
    
    struct SafeArea {
        static var top: CGFloat = 0
        static var bottom: CGFloat = 0
    }
    
    struct NavigationBar {
        static var height: CGFloat = 0
    }
    
    static func setupDeviceSize(_ window: UIWindow?) {
        guard let window = window else { return }
        
        Device.Screen.height = window.frame.height
        Device.Screen.width = window.frame.width
        Device.SafeArea.top = window.safeAreaInsets.top
        Device.SafeArea.bottom = window.safeAreaInsets.bottom
    }
}
